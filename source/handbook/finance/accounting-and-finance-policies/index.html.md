---
layout: markdown_page
title: "Accounting and Finance Policies"
---

## Accounting and Finance Policies

This page contains GitLab's accounting and finance policies.

- [Accrued Liabilities](/handbook/finance/accrued-liabilities-policy/)
- [Capital Assets](/handbook/finance/capital-assets-policy/)
- [Prepaid Expenses](/handbook/finance/prepaid-expense-policy/)
- [Procure-to-Pay](/handbook/finance/procure-to-pay/)